
const bracketValidator=(input = "{([{}])}")=>{
    const inputSplited=input.split("")

    if(inputSplited.length % 2 !== 0) return false

    const SQUARE_BRACKET_OPENED='['
    const SQUARE_BRACKET_CLOSED=']'

    const PARENTHESIS_OPENED='('
    const PARENTHESIS_CLOSED=')'

    const KEY_CLOSED='}'

    const CLOSED_BRACKETS=[
        SQUARE_BRACKET_CLOSED,
        PARENTHESIS_CLOSED,
        KEY_CLOSED
    ]

    if(CLOSED_BRACKETS.includes(inputSplited[0])) return false

    let squareBrackets = 0
    let parenthesis = 0
    let keys = 0

    for(let i=0; i < inputSplited.length; i++){
        if(i< inputSplited.length/2){
            if(inputSplited[i] === SQUARE_BRACKET_OPENED){
                squareBrackets++
            }else if(inputSplited[i] === PARENTHESIS_OPENED){
                parenthesis++
            }else{
                keys++    
            }
        }else{
            if(inputSplited[i] === SQUARE_BRACKET_CLOSED){
                squareBrackets--
            }else if(inputSplited[i] === PARENTHESIS_CLOSED){
                parenthesis--
            }else{
                keys--    
            }
        }
    }

    return squareBrackets === 0 && parenthesis === 0 && keys === 0
}

//input = "{([{}])}"
//output = true

const input1 = "{([{}])}"
const response1 = bracketValidator(input1)

const input2 = "{}"
const response2 = bracketValidator(input2)

const input3 = "](}"
const response3 = bracketValidator(input3)

const input4 = "[]"
const response4 = bracketValidator(input4)

const enter1={
    input1,
    response1
}

const enter2={
    input2,
    response2
}
const enter3={
    input3,
    response3
}
const enter4={
    input4,
    response4
}
console.table([enter1, enter2, enter3, enter4]);





       
    
    


